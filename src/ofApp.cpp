#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    sampleRate = 44100;
    channels = 2;

    ofSetFrameRate(60);
	
	camWidth 		= 320;	// try to grab at this size. 
	camHeight 		= 240;

    fileName = "testMovie";
    fileExt = ".mov";

    // vidRecorder
    vidRecorder.setVideoCodec("mpeg4");
    vidRecorder.setVideoBitrate("800k");
    vidRecorder.setAudioCodec("mp3");
    vidRecorder.setAudioBitrate("192k");

    ofAddListener(vidRecorder.outputFileCompleteEvent, this, &ofApp::recordingComplete);
	
    soundStream.setup(this, 0, channels, sampleRate, 256, 4);

    bRecording = false;
    bNewFrame = false;

    //we can now get back a list of devices. 
	ofVideoGrabber stupid;
	vector<ofVideoDevice> devices = stupid.listDevices();

    for(int i = 0; i < devices.size(); i++){
        if (ofIsStringInString(devices[i].deviceName,"FaceTime") == 0
                && ofIsStringInString(devices[i].deviceName,"BisonCam") == 0) {
		    cout << "cam add? :::::: " << devices[i].id << ": " << devices[i].deviceName; 
            if( devices[i].bAvailable ){
                cout << endl;
		        ofVideoGrabber vidGrabber;
		        vidGrabber.setDeviceID(i);
		    	//vidGrabber.setDesiredFrameRate(30);
		        vidGrabber.initGrabber(camWidth,camHeight);
		        vidGrabbers.push_back(vidGrabber);
            }else{
                cout << " - unavailable " << endl; 
            }
        }
	}
    recordImage.allocate(640,480,OF_IMAGE_COLOR);
    recordFbo.allocate(640,480,GL_RGB);
    recordFbo.begin();
    ofClear(0,255,0);
    recordFbo.end();
	
	ofSetVerticalSync(true);
}

//--------------------------------------------------------------
void ofApp::exit(){
    ofRemoveListener(vidRecorder.outputFileCompleteEvent, this, &ofApp::recordingComplete);
    vidRecorder.close();
}

//--------------------------------------------------------------
void ofApp::update(){
    for (int i = 0; i < vidGrabbers.size(); i++) {
        vidGrabbers[i].update();
    }
    if(bNewFrame && bRecording){
        bool success = vidRecorder.addFrame(recordImage.getPixels());
        if (!success) {
            ofLogWarning("This frame was not added!");
        } else {
            bNewFrame = false;
        }
    }

    // Check if the video recorder encountered any error while writing video frame or audio smaples.
    if (vidRecorder.hasVideoError()) {
        ofLogWarning("The video recorder failed to write some frames!");
    }

    if (vidRecorder.hasAudioError()) {
        ofLogWarning("The video recorder failed to write some audio samples!");
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofSetHexColor(0xffffff);

    recordImage.bind();
    int i = 0;
    for (int i = 0; i < vidGrabbers.size(); i++) {
        if (i == 0)  
            vidGrabbers[i].draw(0,0,ofGetWidth()*0.5,ofGetHeight()*0.5);
        if (i == 1)  
            vidGrabbers[i].draw(ofGetWidth()*0.5,0,ofGetWidth()*0.5,ofGetHeight()*0.5);
        if (i == 2)  
            vidGrabbers[i].draw(0,ofGetHeight()*0.5,ofGetWidth()*0.5,ofGetHeight()*0.5);
        if (i == 3)  
            vidGrabbers[i].draw(ofGetWidth()*0.5,ofGetHeight()*0.5,ofGetWidth()*0.5,ofGetHeight()*0.5);
    }
    recordImage.unbind();

    if(bRecording){
        ofSetColor(255, 0, 0);
        ofDrawCircle(ofGetWidth() - 20, 20, 5);
    }
    bNewFrame = true;
}

//--------------------------------------------------------------
void ofApp::audioIn(float *input, int bufferSize, int nChannels){
    if(bRecording)
        vidRecorder.addAudioSamples(input, bufferSize, nChannels);
}

//--------------------------------------------------------------
void ofApp::recordingComplete(ofxVideoRecorderOutputFileCompleteEventArgs& args){
    cout << "The recoded video file is now complete." << endl;
}

//--------------------------------------------------------------
void ofApp::keyPressed  (int key){ 

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){ 
    if(key=='r'){
        bRecording = !bRecording;
        if(bRecording && !vidRecorder.isInitialized()) {
            vidRecorder.setup(fileName+ofGetTimestampString()+fileExt, recordImage.getWidth(), recordImage.getHeight(), 30, sampleRate, channels);
//          vidRecorder.setup(fileName+ofGetTimestampString()+fileExt, vidGrabber.getWidth(), vidGrabber.getHeight(), 30); // no audio
//            vidRecorder.setup(fileName+ofGetTimestampString()+fileExt, 0,0,0, sampleRate, channels); // no video
//          vidRecorder.setupCustomOutput(vidGrabber.getWidth(), vidGrabber.getHeight(), 30, sampleRate, channels, "-vcodec mpeg4 -b 1600k -acodec mp2 -ab 128k -f mpegts udp://localhost:1234"); // for custom ffmpeg output string (streaming, etc)

            // Start recording
            vidRecorder.start();
        }
        else if(!bRecording && vidRecorder.isInitialized()) {
            vidRecorder.setPaused(true);
        }
        else if(bRecording && vidRecorder.isInitialized()) {
            vidRecorder.setPaused(false);
        }
    }
    if(key=='c'){
        bRecording = false;
        vidRecorder.close();
    }
	
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
	
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
	
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
